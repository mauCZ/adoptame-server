
drop index  ADOPCION_PK;

drop table  ADOPCION;

/*==============================================================*/
/* Table:  ADOPCION                                              */
/*==============================================================*/
create table  ADOPCION (
   ID                   INT4               not null,
   NOMBRE	        TEXT               not null,
   ESPECIE              TEXT               not null,
   EDAD                 TEXT,
   FECHA                TEXT,                 
   SEXO                 TEXT,
   RAZA                 TEXT,
   TAMA�O               TEXT,
   PESO                 INT2,
   VACUNADO             TEXT,
   SANO                 TEXT,
   ESTERILIZADO         TEXT,
   DIRECCION            TEXT,
   DESCRIPCION          TEXT,
   TIPOIMAGEN           TEXT,
   RUTAIMAGEN		TEXT,
   constraint PK_ADOPCION primary key (ID)
);

/*==============================================================*/
/* Index:  ADOPCION_PK                                            */
/*==============================================================*/
create unique index  ADOPCION_PK on  ADOPCION (
ID
);
