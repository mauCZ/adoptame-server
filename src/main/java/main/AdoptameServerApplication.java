package main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import main.adopcion.Adopcion;

@SpringBootApplication
public class AdoptameServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdoptameServerApplication.class, args);

	}

}
