package main.adopcion;

import org.springframework.data.repository.CrudRepository;

public interface AdopcionRepository extends CrudRepository<Adopcion,Integer>{
  //En esta interfaz esta todas las custom queries, o queries propias que no esten implementadas en spring
  //crudrepository tiene las operaciones basicas CRUD
}
