package main.adopcion;

import java.util.UUID;

import javax.persistence.*;

//import org.springframework.boot.autoconfigure.domain.EntityScan;

@Entity
public class Adopcion {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  /* Campos:
   * nombre edad especie tamanio raza sexo vacunado esterelizado direccion descripcion fecha
   * 
   * */
  private String nombre;
  private String edad;
  private String especie;
  public Adopcion() {
	
  }
  public Adopcion(int id, String nombre, String edad, String especie, String tamaño, String raza, String sexo,
	  String vacunado, String esterelizado, String peso, String direccion, String descripcion, String fecha,
	  String rutaImagen) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.edad = edad;
	this.especie = especie;
	this.tamaño = tamaño;
	this.raza = raza;
	this.sexo = sexo;
	this.vacunado = vacunado;
	this.esterelizado = esterelizado;
	this.peso = peso;
	this.direccion = direccion;
	this.descripcion = descripcion;
	this.fecha = fecha;
	this.rutaImagen = rutaImagen;
  }
  public Adopcion(String nombre, String edad, String especie, String tamaño, String raza, String sexo, String vacunado,
	  String esterelizado, String peso, String direccion, String descripcion, String fecha, String rutaImagen) {
	super();
	this.nombre = nombre;
	this.edad = edad;
	this.especie = especie;
	this.tamaño = tamaño;
	this.raza = raza;
	this.sexo = sexo;
	this.vacunado = vacunado;
	this.esterelizado = esterelizado;
	this.peso = peso;
	this.direccion = direccion;
	this.descripcion = descripcion;
	this.fecha = fecha;
	this.rutaImagen = rutaImagen;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getNombre() {
    return nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public String getEdad() {
    return edad;
  }
  public void setEdad(String edad) {
    this.edad = edad;
  }
  public String getEspecie() {
    return especie;
  }
  public void setEspecie(String especie) {
    this.especie = especie;
  }
  public String getTamaño() {
    return tamaño;
  }
  public void setTamaño(String tamaño) {
    this.tamaño = tamaño;
  }
  public String getRaza() {
    return raza;
  }
  public void setRaza(String raza) {
    this.raza = raza;
  }
  public String getSexo() {
    return sexo;
  }
  public void setSexo(String sexo) {
    this.sexo = sexo;
  }
  public String getVacunado() {
    return vacunado;
  }
  public void setVacunado(String vacunado) {
    this.vacunado = vacunado;
  }
  public String getEsterelizado() {
    return esterelizado;
  }
  public void setEsterelizado(String esterelizado) {
    this.esterelizado = esterelizado;
  }
  public String getPeso() {
    return peso;
  }
  public void setPeso(String peso) {
    this.peso = peso;
  }
  public String getDireccion() {
    return direccion;
  }
  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getFecha() {
    return fecha;
  }
  public void setFecha(String fecha) {
    this.fecha = fecha;
  }
  public String getRutaImagen() {
    return rutaImagen;
  }
  public void setRutaImagen(String rutaImagen) {
    this.rutaImagen = rutaImagen;
  }
  private String tamaño;
  private String raza;
  private String sexo;
  private String vacunado;
  private String esterelizado;
  private String peso;
  private String direccion;
  private String descripcion;
  private String fecha;
  private String rutaImagen;
  
}
