package main.adopcion;
import java.awt.PageAttributes.MediaType;
import java.io.*;

import org.hibernate.result.Outputs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
@Service
public class AdopcionService {
  
  @Autowired
  private AdopcionRepository adopcionRepository;
  
  
  public List<Adopcion> getAdopciones(){
	List<Adopcion> adopciones = new ArrayList<>();
	adopcionRepository.findAll().forEach(adopciones::add);
	return adopciones;
  }
  
  public void addAdopcion(Adopcion a) throws IOException{
	
	adopcionRepository.save(a);
  }
  public Adopcion getAdopcion(int id) {
	Optional<Adopcion> adop = adopcionRepository.findById(id); 
	if(adop.isPresent()) return adop.get();
	return null;
  }
  public void deleteAdopcion(int id) {
	adopcionRepository.deleteById(id);
  }
  public void deleteAllAdopciones() {
	adopcionRepository.deleteAll();
  }
}
