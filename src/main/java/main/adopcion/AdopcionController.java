package main.adopcion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.awt.PageAttributes.MediaType;
import java.io.*;
import java.nio.file.Files;

import ch.qos.logback.core.status.Status;

import java.util.*;
@RestController
@CrossOrigin(origins="*")
public class 	AdopcionController {
  @Autowired
  private AdopcionService adopcionService;
  
  @GetMapping("/")
  public String ini(){
	return "Hola bievenido al servidor de adoptame XD";
  }
  
  @GetMapping("/adopcion")
  public List<Adopcion> getAdopciones(){
	return adopcionService.getAdopciones();
  }
  
  //retorn un objeto Adopcion
  @GetMapping("/adopcion/{id}")
  public ResponseEntity<?> getAdopcion(@PathVariable int id) {
	Adopcion a = adopcionService.getAdopcion(id);
	if(a == null) return new ResponseEntity<>("La adopcion con el id "+id+" no existe.",HttpStatus.BAD_REQUEST);
	
	System.out.println("adopcion existe");
	return new ResponseEntity<>(a,HttpStatus.OK);
  }	
  
  @GetMapping("/adopcion/imagen/{id}")
  public ResponseEntity<?> getImagen(@PathVariable int id) throws IOException{
	Adopcion a = adopcionService.getAdopcion(id);
	if(a == null) {
	  return new ResponseEntity<>("La adopcion con id "+id+" no existe.",HttpStatus.NOT_FOUND);
	}
	String absP = a.getRutaImagen();
	File img = new File(absP);
	HttpHeaders hh = new HttpHeaders();
	
//	hh.add("Content-Type", "multipart/form-data");
	hh.add(HttpHeaders.CONTENT_LENGTH,  String.valueOf(img.length()));
	
	System.out.println("debug "+absP+" "+img.exists());
	
	return new ResponseEntity<byte[]>(Files.readAllBytes(img.toPath()), HttpStatus.ACCEPTED);
  }	

  
  @DeleteMapping("/adopcion/{id}")
  public ResponseEntity<?> deleteAdopcion(@PathVariable int id) {
	Adopcion a = adopcionService.getAdopcion(id);
	if(a == null) return new ResponseEntity<>("La adopcion con id "+id+" ni siquiera existe.",HttpStatus.NO_CONTENT);
	String ruta = a.getRutaImagen();
	File img = new File(ruta);
	img.delete();
   	adopcionService.deleteAdopcion(id);
   	return new ResponseEntity<>(HttpStatus.OK);
  }
  
  @DeleteMapping("/adopcion")
  public void deleteAllAdopciones() {
	String abs = new File("").getAbsolutePath();
	File img = new File(abs+File.separator+"imagenes");
	String[] imagenes = img.list();
	for(String str : imagenes) {
	  if(!str.equals("prueba.png")) {
		File f = new File(abs+File.separator+"imagenes"+File.separator+str);
		f.delete();
	  }
	}
    adopcionService.deleteAllAdopciones();
  }
  
  
  @PostMapping("/adopcion")
  public ResponseEntity<?> addAdopciones(String nombre,String edad,String especie,String tamaño,
  	  								String raza, String sexo, String vacunado,String esterelizado,
  	  								String peso,String direccion,String descripcion,String fecha,MultipartFile imagen)
  									throws IOException{
	if(imagen.isEmpty()) return new ResponseEntity<>("falta imagen", HttpStatus.BAD_REQUEST);
	Adopcion a = new Adopcion(nombre,edad,especie,tamaño,raza,sexo,vacunado,esterelizado,peso,direccion,descripcion,fecha,"");
	String absP = new File("").getAbsolutePath();
	String tipo = imagen.getContentType();
	File img = new File(absP+File.separator+"imagenes"+File.separator+imagen.getOriginalFilename());
	OutputStream os = new FileOutputStream(img);
	os.write(imagen.getBytes());
	os.close();
	a.setRutaImagen(img.getAbsolutePath());
	adopcionService.addAdopcion(a);
	
	System.out.println(absP+File.separator+"imagenes"+File.separator+imagen.getOriginalFilename());
	System.out.println(imagen.getOriginalFilename() + " " + imagen.getContentType());
		return new ResponseEntity<>(a.getId(), HttpStatus.OK);
  }
}
